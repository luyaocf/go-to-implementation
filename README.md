# GoToImplementation

# 1 介绍
GoToImplementation 是一款跳转到各类实现的 IntelliJ IDEA 插件。日常开发中常见的跳转有单一模块间接口到实现跳转、 Mybatis 的 Mapper 到 xml 跳转、由于微服务流行起来的使用 @FeignClient 注解的不同模块间跳转。本插件致力于实现第二、三种跳转功能。

# 2 软件原理
## 2.1 Mapper 接口跳转到 Xml 文件原理
- 根据光标所在的位置，获取该增删改查方法的接口文件，如 UserMapper
- 全局搜索 UserMapper.xml 文件，如果未找到，则搜索 User.xml 文件
- 找到 Xml 文件后，根据光标所在的增删改查方法名，逐行判断 Xml 是否存在该内容，存在则跳转 

## 2.2 @FeignClient 方法的跳转原理
- 根据光标所在方法名，找到该方法所被声明的，被 @FeignClient 注解标注的类
- 获取 @FeignClient 注解的相关属性，如表示模块名的 name 属性，表示 Controller 路径的 path 属性等，
这里要注意的是，如果模块名和 name 属性不一致，则可能导致无法跳转
- 获取该方法上的 @RequestMapping 注解，拿到 path 属性
- 将两个 path 属性值拼接起来，遍历模块中所有被 @Controller 或 @RestController 注解标注的类，
对比两个 path 属性拼接值，和该 Controller 中的方法是否匹配，如果匹配则跳转 

# 3 安装
- 方式一：在插件商店直接搜索 Go To Impl 即可
- 方式二：选择合适自已的版本，在本 git 仓库切换到对应的分支，下载后解压打开，安装根目录下的 jar 文件即可。

# 4 使用
- 微服务 Feign 接口与实现的相互跳转
![ms](./imgs/ms.gif)

- MyBatis 的 Mapper.java 与 Mapper.xml 相互跳转：
![ms](./imgs/mapper.gif)

> Ctrl + Alt + I：跳转到 Feign 接口实现，或 MyBatis 的 Mapper.xml 文件

> Ctrl + Alt + D：跳转到 Feign 接口声明，或 MyBatis 的 Mapper.java 文件

# 版本 
- V1.2.21
  - 适配 IU-243(2024.3)
- V1.2.20
  - 适配 IU-242(2024.2)
  - fix 1 usage of experimental API of PsiExternalReferenceHost in plugin com.intellij.modules.java
- V1.2.19
  - 适配 IU-241(2024.1)
- V1.2.18
  - 适配 IU-232(2023.2)
- V1.2.17
  - 适配 IU-231.8109.175(2023.1)
- V1.2.16
  - 适配 IU-231.4840.387(2023.1)
- V1.2.15
  - 适配 IU-223.8214.52(2022.3.1)
- V1.2.14
  - 适配 IU-223.7571.123(2022.3)
- V1.2.13
  - 适配 IU-222.4459.24(2022.2.4)
- V1.2.12
  - 适配 IU-222.4345.14(2022.2.3)
- V1.2.11
  - 适配 IU-222.4167.29(2022.2.2)
- V1.2.10
  - 支持 @Mapper 注解跳转
  - 适配 IU-221.5591.52(2022.1.1)
- V1.2.9
  - 新增 Feign 接口跳转时支持 @DeleteMapping 和 @PutMapping
- v1.2.8
  - Mapper 接口跳转到 Mapper.xml 文件，当 Mapper 接口以 "Dao" 结尾，但 Mapper.xml 没有以 "Dal" 结尾。 
  - 支持 2021.2.2(IU-221.4906)
- v1.2.7
  - Mapper 接口跳转到 Mapper.xml 文件，即使没有 @Repository 使用注解。 
  - 支持 2021.2.2(IU-212.5284.40)
- v1.2.6
    - 支持 2021.2.2(IU-212.5284.40)
- V1.2.3
    - 适配 2021.1.3(IU-211.7628.21)
- V1.2.2 
    - 修复bug：控制器上没有 @RequestMapping 注解时无法跳转问题
    - 修复bug：控制器的方法上没有 @RequestMapping 注解，而是 @GetMapping 或 @PostMapping 注解时无法跳转问题

- V1.2.1
    - 由于 IU-202.8194.7(2020.2.4) 版本后，通知相关的 API 类 NotificationGroup 被标注为过时，因此 V1.1.3 只能支持到 2020.2.4。而本版本 V1.2.1 将从 2020.2.4 后开始支持，即 IU-203.5251.39 版本开始支持。

- V1.1.x 
    - 适用于 IntelliJ IDEA 182.5107 —— 202.8194

- V1.0.x
    - 适用于 IntelliJ IDEA 171.4694 —— 181.5684

