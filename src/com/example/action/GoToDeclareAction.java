package com.example.action;

import com.example.execute.Execute;
import com.example.execute.GoToDeclareExecute;
import com.example.handler.declare.DefaultGoToDeclareHandler;
import com.example.handler.declare.FeignImplGoToDeclareHandler;
import com.example.handler.declare.XmlGoToDeclareHandler;
import com.example.util.PsiElementUtil;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.LangDataKeys;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import org.jetbrains.annotations.NotNull;

/**
 * @author Aaron
 * @since 2021/8/29 13:37
 * <p>描述：</p>
 */
public class GoToDeclareAction extends AnAction {

    @Override
    public void actionPerformed(@NotNull AnActionEvent anActionEvent) {
        // Feign 接口中定义方法的实现
        PsiElement currentPsiElement = anActionEvent.getData(LangDataKeys.PSI_ELEMENT);
        if (PsiElementUtil.isFeignImplMethod(currentPsiElement)) {
            Execute execute = new GoToDeclareExecute(new FeignImplGoToDeclareHandler(anActionEvent, currentPsiElement));
            execute.doExecute();
            return;
        }

        // Xml 映射文件
        PsiFile psiFile = anActionEvent.getData(PlatformDataKeys.PSI_FILE);
        if (PsiElementUtil.isMapperXml(psiFile)) {
            Execute execute = new GoToDeclareExecute(new XmlGoToDeclareHandler(anActionEvent, psiFile));
            execute.doExecute();
            return;
        }

        // 默认
        Execute execute = new GoToDeclareExecute(new DefaultGoToDeclareHandler(anActionEvent, psiFile));
        execute.doExecute();

    }

}
