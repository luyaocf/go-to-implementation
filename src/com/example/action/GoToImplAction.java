package com.example.action;

import com.example.execute.Execute;
import com.example.execute.GoToImplExecute;
import com.example.handler.impl.DefaultGoToImplHandler;
import com.example.handler.impl.FeignMethodGoToImplHandler;
import com.example.handler.impl.MapperGoToImplHandler;
import com.example.util.CustomNotifier;
import com.example.util.PsiElementUtil;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.LangDataKeys;
import com.intellij.psi.*;
import com.intellij.psi.util.PsiUtil;

import java.util.Objects;

/**
 * @author Aaron
 * @since 2020/11/6 13:28
 * <p>描述：</p>
 */
public class GoToImplAction extends AnAction {

    @Override
    public void actionPerformed(AnActionEvent anActionEvent) {
        PsiElement currentPsiElement = anActionEvent.getData(LangDataKeys.PSI_ELEMENT);
        if (currentPsiElement == null) {
            CustomNotifier.error(anActionEvent.getProject(), "No elements are selected!");
            return;
        }

        // 根据本插件的目标，这里被选择的元素可以是以下几种情况：
        // 1 Feign 接口上的方法，跳转到本模块或其它模块的接口方法实现上
        // 2 Mapper 接口，跳转到对应的 xml 文件
        // 3 Mapper 接口上的方法，跳转到对应的 xml 文件的 sql 上

        Execute execute;
        if (PsiElementUtil.isFeignMethod(currentPsiElement)) {
            // Feign 方法跳转处理
            execute = new GoToImplExecute(new FeignMethodGoToImplHandler(anActionEvent, currentPsiElement));
        } else if (PsiElementUtil.isMapperInterface(currentPsiElement)) {
            // Mapper 接口跳转处理
            execute = new GoToImplExecute(new MapperGoToImplHandler(anActionEvent,
                    (PsiClass) currentPsiElement, null));
        } else if (PsiElementUtil.isMapperInterfaceField(currentPsiElement)) {
            // Mapper 对象变量跳转处理：获取其类型，然后转化为 PsiClass
            PsiTypeElement psiTypeElement = ((PsiField) currentPsiElement).getTypeElement();
            PsiClass psiClass = PsiUtil.resolveClassInType(Objects.requireNonNull(psiTypeElement).getType());
            execute = new GoToImplExecute(new MapperGoToImplHandler(anActionEvent, psiClass, null));
        } else if (PsiElementUtil.isMapperMethod(currentPsiElement)) {
            // Mapper 接口方法跳转
            execute = new GoToImplExecute(new MapperGoToImplHandler(anActionEvent,
                    (PsiClass) currentPsiElement.getParent(), (PsiMethod) currentPsiElement));
        } else {
            // 其它情况处理
            execute = new GoToImplExecute(new DefaultGoToImplHandler(anActionEvent, currentPsiElement));
        }

        execute.doExecute();
    }

}
