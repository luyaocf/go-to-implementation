package com.example.handler.impl;

import com.example.util.CustomNotifier;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleUtil;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.search.PsiShortNamesCache;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * @author Aaron
 * @since 2020/11/8 16:21
 * <p>描述：</p>
 */
public class MapperGoToImplHandler extends BaseGoToImplHandler {
    private static final String MAPPER = "Mapper";
    private static final String DAO = "Dao";

    private final AnActionEvent anActionEvent;
    private final PsiClass currentPsiClass;
    private final PsiMethod currentPsiMethod;

    public MapperGoToImplHandler(AnActionEvent anActionEvent, PsiClass currentPsiClass, PsiMethod currentPsiMethod) {
        this.anActionEvent = anActionEvent;
        this.currentPsiClass = currentPsiClass;
        this.currentPsiMethod = currentPsiMethod;
    }

    @Override
    public void doHandle() {
        super.doHandle();

        // 获取当前 Project
        Project project = anActionEvent.getProject();
        if (project == null) {
            System.out.println("Can't get the project by the action event.");
            return;
        }

        String mapperClassName = currentPsiClass.getName();
        if (StringUtils.isBlank(mapperClassName)) {
            CustomNotifier.error(anActionEvent.getProject(), "Can't get the name of the action's element.");
            return;
        }

        // 根据名称查找映射文件
        String mapperName = mapperClassName.replaceAll(MAPPER, "").replaceAll(DAO, "");
        PsiFile[] mapperXmlPsiFiles = PsiShortNamesCache.getInstance(project).getFilesByName(mapperName + "Mapper.xml");
        if (mapperXmlPsiFiles.length > 0) {
            CustomNotifier.info(anActionEvent.getProject(), "from " + mapperClassName + " to " + mapperName + "Mapper.xml");
            this.doLocate(project, mapperXmlPsiFiles);
        }

        mapperXmlPsiFiles = PsiShortNamesCache.getInstance(project).getFilesByName(mapperName + ".xml");
        if (mapperXmlPsiFiles.length > 0) {
            CustomNotifier.info(anActionEvent.getProject(), "from " + mapperClassName + " to " + mapperName + ".xml");
            this.doLocate(project, mapperXmlPsiFiles);
        }

        mapperXmlPsiFiles = PsiShortNamesCache.getInstance(project).getFilesByName(mapperName + "Dao.xml");
        if (mapperXmlPsiFiles.length > 0) {
            CustomNotifier.info(anActionEvent.getProject(), "It will go to the Mapper Xml of " + mapperName + "Dao.xml");
            this.doLocate(project, mapperXmlPsiFiles);
        }

    }

    private void doLocate(Project project, PsiFile[] mapperXmlPsiFiles) {
        if (project == null || anActionEvent.getProject() == null) {
            return;
        }

        Module module = ModuleUtil.findModuleForFile(currentPsiClass.getContainingFile().getVirtualFile(), anActionEvent.getProject());
        for (PsiFile file : mapperXmlPsiFiles) {
            if (Objects.equals(module, ModuleUtil.findModuleForFile(file.getVirtualFile(), anActionEvent.getProject()))) {
                doLocate(project, currentPsiMethod == null ? "mapper namespace" : currentPsiMethod.getName(), file);
            }
        }
    }

}
