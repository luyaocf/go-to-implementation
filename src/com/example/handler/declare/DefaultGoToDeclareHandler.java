package com.example.handler.declare;

import com.example.util.CustomNotifier;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.psi.PsiFile;

/**
 * @author Aaron
 * @since 2021/8/30 20:07
 * <p>描述：</p>
 */
public class DefaultGoToDeclareHandler extends BaseGoToDeclareHandler {

    private final AnActionEvent anActionEvent;
    private final PsiFile psiFile;

    public DefaultGoToDeclareHandler(AnActionEvent anActionEvent, PsiFile psiFile) {
        this.anActionEvent = anActionEvent;
        this.psiFile = psiFile;
    }

    @Override
    public void doHandle() {
        super.doHandle();
        CustomNotifier.error(anActionEvent.getProject(), "Unsupported Selected in " + psiFile.getName());
    }
}
