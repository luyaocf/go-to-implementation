package com.example.execute;

import com.example.handler.Handler;

/**
 * @author Aaron
 * @since 2021/8/29 14:01
 * <p>描述：</p>
 */
public class GoToDeclareExecute extends BaseExecute {

    private final Handler handler;

    public GoToDeclareExecute(Handler handler) {
        this.handler = handler;
    }

    @Override
    public void doExecute() {
        handler.doHandle();
    }

}
