package com.example.execute;

import com.example.handler.Handler;

/**
 * @author Aaron
 * @since 2020/11/8 16:54
 * <p>描述：</p>
 */
public class GoToImplExecute extends BaseExecute {

    private final Handler handler;

    public GoToImplExecute(Handler handler) {
        this.handler = handler;
    }

    @Override
    public void doExecute() {
        handler.doHandle();
    }

}
