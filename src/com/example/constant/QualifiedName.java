package com.example.constant;

/**
 * @author Aaron
 * @since 2020/11/7 11:04
 * <p>描述：限定名，即全类名</p>
 */
public interface QualifiedName {

    interface Annotation {
        /**
         * '@RequestMapping' 注解
         */
        String REQUEST_MAPPING = "org.springframework.web.bind.annotation.RequestMapping";
        /**
         * '@PostMapping' 注解
         */
        String POST_MAPPING = "org.springframework.web.bind.annotation.PostMapping";
        /**
         * '@GetMapping' 注解
         */
        String GET_MAPPING = "org.springframework.web.bind.annotation.GetMapping";
        /**
         * '@PutMapping' 注解
         */
        String PUT_MAPPING = "org.springframework.web.bind.annotation.PutMapping";
        /**
         * '@DeleteMapping' 注解
         */
        String DELETE_MAPPING = "org.springframework.web.bind.annotation.DeleteMapping";
        /**
         * '@FeignClient' 注解
         */
        String FEIGN_CLIENT = "org.springframework.cloud.openfeign.FeignClient";
        /**
         * '@RestController' 注解
         */
        String REST_CONTROLLER = "org.springframework.web.bind.annotation.RestController";
        /**
         * '@Controller' 注解
         */
        String CONTROLLER = "org.springframework.stereotype.Controller";
        /**
         * '@Repository' 注解
         */
        String REPOSITORY = "org.springframework.stereotype.Repository";
        /**
         * MyBatis 的 @Mapper 注解
         */
        String MAPPER = "org.apache.ibatis.annotations.Mapper";

    }


}
